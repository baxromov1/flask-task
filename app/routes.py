

from flask import jsonify, Flask
from flask_restful import Api, Resource

from .models.cats import Cat

app = Flask(__name__)

api = Api(app)


class CatView(Resource):

    def get(self):
        cats = Cat.query.all()
        output = []
        for cat in cats:
            curr_cat = {}
            curr_cat['cat'] = cat.id
            curr_cat['name'] = cat.name
            curr_cat['description'] = cat.description
            curr_cat['breed'] = cat.breed
            curr_cat['images'] = cat.images.url
            curr_cat['age'] = cat.age

            output.append(curr_cat)
        return jsonify(output)


api.add_resource(CatView, '/cats')

if __name__ == '__main__':
    app.run()
    app.run(debug=True)
