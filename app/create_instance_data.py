import sys

from app.models.cats import Cat, Base, engine, ImagesCat, session_global


def insert_cat(session):
    products_list = [
        (
            'Felidae',
            'cat, (Felis catus), also called house cat or domestic cat, domesticated member of the family Felidae, order Carnivora'
            'Himalayan cat',
            4
        ),
        (
            'Name 1',
            'Abyssinians are highly intelligent and intensely inquisitive cats.',
            'Abyssinian Cat',
            5
        ),
        (
            'Name 2',
            'Abyssinians are highly intelligent and intensely inquisitive cats.',
            'Birman Cat Breed',
            2
        ),
    ]
    for product_tuple in products_list:
        session.add(Cat(*product_tuple))
    session.commit()


def insert_images_cat(session):
    cat = session.query(Cat.id).filter(
        Cat.name == 'Name 1'
    ).first()
    cat_id = cat[0]
    images = [
        (cat_id, 'cat.jpg', 0),
    ]
    for image in images:
        session.add(ImagesCat(*image))
    session.commit()


def main(session):
    Base.metadata.create_all(engine)
    insert_cat(session)
    insert_images_cat(session)


if __name__ == '__main__':
    sys.exit(main(session_global))
