from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session, relationship
from flask import json
from app.logger import logger
from app.settings import config

databases = config.get('DATABASES')

if not databases:
    logger.error('В конфиге не описана БД')

default_db = databases.get('default')
if not default_db:
    logger.error('Проблемы с конфигом БД. Надо смотреть settings.py и конфиг')

engine = create_engine(
    '{0}://{1}:{2}@{3}:{4}/{5}'.format(
        default_db['ENGINE'], default_db['USER'], default_db['PASSWORD'],
        default_db['HOST'], default_db['PORT'], default_db['DB']
    ),
    json_serializer=json.dumps,
    echo=False,
)

logger.debug('Создали engine')
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
# Используем не общеупотребимое имя session, что б было меньше соблазна
# пользоваться глобальной переменной напрямую
session_global = Session()
logger.debug('Создали session')

Base = declarative_base()
Base.query = Session.query_property()
logger.debug('Создали Base')

Base = declarative_base()
Base.query = Session.query_property()
logger.debug('Создали Base')


class ImagesCat(Base):
    __tablename__ = 'images_cat'
    id = Column(Integer, primary_key=True)
    id_cat = Column(Integer, ForeignKey('cat.id'))
    file = Column(String(200))
    priority = Column(Integer)

    cat = relationship('Cat', back_populates='images')

    def __init__(self, id_cat, file, priority=10):
        self.id_cat = id_cat
        self.file = file
        self.priority = priority

    def __repr__(self):
        return '<images_cat({0})>'.format(self.file)


class Cat(Base):
    __tablename__ = 'cat'
    id = Column(Integer, primary_key=True)
    name = Column(String(200))
    description = Column(String)
    breed = Column(String(200), unique=True)
    age = Column(Integer)
    images = relationship('ImagesProduct', back_populates='product')

    def __init__(self, name, description, breed, age):
        self.name = name
        self.description = description
        self.breed = breed
        self.age = age

